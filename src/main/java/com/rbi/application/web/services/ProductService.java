package com.rbi.application.web.services;

import com.rbi.application.web.model.Product;
import com.rbi.application.web.reposiories.ProductRepository;
import com.rbi.application.web.exceptions.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public List<Product> findAll() {
        return repository.findAll();
    }

    public Product findById(Long id) throws ProductNotFoundException {
        return repository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    public Product create(Product resource) {
        return repository.save(resource);
    }

    public Product update(Long id, Product resource) throws ProductNotFoundException {
        return repository.findById(id)
                .map(product -> {
                    product.setName(resource.getName());
                    product.setColor(resource.getColor());
                    return repository.save(product);})
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    public void deleteById(Long id) throws ProductNotFoundException {
        findById(id);
        repository.deleteById(id);
    }
}
