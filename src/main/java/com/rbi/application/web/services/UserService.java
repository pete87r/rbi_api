package com.rbi.application.web.services;

import com.rbi.application.web.model.User;
import com.rbi.application.web.reposiories.UserRepository;
import com.rbi.application.web.exceptions.UserNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public List<User> findAll() {
        return repository.findAll();
    }

    public User findById(Long id) throws UserNotFoundException {
         return repository.findById(id)
                 .orElseThrow(() -> new UserNotFoundException(id));
    }

    public User create(User resource) {
        return repository.save(resource);
    }

    public User update(Long id, User resource) throws UserNotFoundException {
        return repository.findById(id)
                .map(user -> {
                        user.setName(resource.getName());
                        user.setAddress(resource.getAddress());
                        return repository.save(user);})
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    public void deleteById(Long id) throws UserNotFoundException {
        findById(id);
        repository.deleteById(id);
    }
}
