package com.rbi.application.web.controllers;

import com.rbi.application.web.model.User;
import com.rbi.application.web.exceptions.UserNotFoundException;
import com.rbi.application.web.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/users")
public class UserController {

    UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    public UserController() {
    }

    @GetMapping
    public List<User> findAll() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    public User findById(@PathVariable("id") Long id) throws UserNotFoundException {
        return service.findById(id);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public User create(@RequestBody User resource) {
        return service.create(resource);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(OK)
    public User update(@PathVariable( "id" ) Long id, @RequestBody User resource) throws UserNotFoundException {
        return service.update(id, resource);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(OK)
    public void delete(@PathVariable("id") Long id) throws UserNotFoundException {
        service.deleteById(id);
    }
}
