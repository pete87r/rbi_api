package com.rbi.application.web.controllers;

import com.rbi.application.web.model.Product;
import com.rbi.application.web.exceptions.ProductNotFoundException;
import com.rbi.application.web.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/products")
public class ProductController {

    ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping
    public List<Product> findAll() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    public Product findById(@PathVariable("id") Long id) throws ProductNotFoundException {
        return service.findById(id);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Product create(@RequestBody Product resource) {
        return service.create(resource);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(OK)
    public Product update(@PathVariable( "id" ) Long id, @RequestBody Product resource) throws ProductNotFoundException {
        return service.update(id, resource);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(OK)
    public void delete(@PathVariable("id") Long id) throws ProductNotFoundException {
        service.deleteById(id);
    }
}
