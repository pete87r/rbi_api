INSERT INTO user (id, name, address) VALUES ('1111', 'Jim', 'London');
INSERT INTO user (id, name, address) VALUES ('2222', 'Tom', 'Paris');
INSERT INTO user (id, name, address) VALUES ('3333', 'Jack', 'New York');
INSERT INTO user (id, name, address) VALUES ('444', 'Brenda', 'Chicago');
INSERT INTO user (id, name, address) VALUES ('5555', 'Tod', 'Boston');
INSERT INTO product (id, name, color) VALUES ('1234', 'car', 'red');
INSERT INTO product (id, name, color) VALUES ('2345', 'bicycle', 'blue');
INSERT INTO product (id, name, color) VALUES ('3456', 'boat', 'black');
INSERT INTO product (id, name, color) VALUES ('4567', 'rocket', 'green');
INSERT INTO product (id, name, color) VALUES ('5678', 'bike', 'white');

