package com.rbi.application.web.services

import com.rbi.application.web.model.Product
import com.rbi.application.web.reposiories.ProductRepository
import spock.lang.Specification

class ProductServiceSpec extends Specification {

    ProductService service

    ProductRepository repository = Mock()

    def setup() {
        service = new ProductService(repository)
    }

    def 'should find all users from repository'() {
        given:
        List<Product> expected = [new Product('car', 'black'), new Product('car', 'blue')]

        when:
        List<Product> actual = service.findAll()

        then:
        1 * repository.findAll() >> expected

        and:
        expected == actual
    }

    def 'should create new user'() {
        given:
        Product expected = new Product(id: 123, name:  'car', color:  'red')

        when:
        def actual = service.create(expected)

        then:
        1 * repository.save(expected) >> expected

        and:
        expected == actual
    }
}
