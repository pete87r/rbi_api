package com.rbi.application.web.services

import com.rbi.application.web.exceptions.UserNotFoundException
import com.rbi.application.web.model.User
import com.rbi.application.web.reposiories.UserRepository
import spock.lang.Specification

class UserServiceSpec extends Specification {

    UserService service

    UserRepository repository = Mock()

    def setup() {
        service = new UserService(repository)
    }

    def 'should find all users from repository'() {
        given:
        List<User> expected = [new User('Tom', 'London'), new User('Jim', 'Miami')]

        when:
        List<User> actual = service.findAll()

        then:
        1 * repository.findAll() >> expected

        and:
        expected == actual
    }

    def 'should create new user'() {
        given:
        User expected = new User(id: 123, name:  'Tom', address:  'London')

        when:
        def actual = service.create(expected)

        then:
        1 * repository.save(expected) >> expected

        and:
        expected == actual
    }
}
